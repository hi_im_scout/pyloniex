#anewkirk

from time import time
import urllib.request as urlreq
import urllib.parse as parse
import json
import hmac
import hashlib

base_public_url = 'https://poloniex.com/public?command='
trading_url = 'https://poloniex.com/tradingApi'
json_dec = json.JSONDecoder()

def pub_api_request(command):
    req = urlreq.Request(base_public_url + command)
    return req

def decode_json_response(r):
    return json_dec.decode(r.read().decode('utf-8'))

def req_to_dict(req):
    response = urlreq.urlopen(req)
    return decode_json_response(response)

def command_param(command):
    return {'command':command}

class PoloniexClient():
    def __init__(self, key, secret):
        self.key = key
        self.secret = secret

    #Public API#
    @staticmethod
    def return_ticker():
        req = pub_api_request('returnTicker')
        return req_to_dict(req)
    
    @staticmethod
    def return_24h_vol():
        req = pub_api_request('return24hVolume')
        return req_to_dict(req)

    @staticmethod
    def return_order_book(pair = 'all', depth = 10):
        rstr = 'returnOrderBook&currencyPair=' + pair
        rstr += '&depth=' + str(depth)
        req = pub_api_request(rstr)
        return req_to_dict(req)

    @staticmethod
    def return_pair_trade_history(pair, ranged, start = int(time()) - 1000, end = int(time())):
        rstr = 'returnTradeHistory&currencyPair=' + pair
        if ranged:
            rstr += '&start=' + str(start)
            rstr += '&end=' + str(end)
        req = pub_api_request(rstr)
        return req_to_dict(req)

    @staticmethod
    def return_chart_data(pair, period, start, end):
        '''Start and end times are in the form of 
        UNIX timestamps. Period value must be 
        one of the following:
        300, 900, 1800, 7200, 14400, 86400'''
        
        rstr = 'returnChartData&currencyPair=' + pair
        rstr += '&start=' + str(start)
        rstr += '&end=' + str(end)
        rstr += '&period=' + str(period)
        req = pub_api_request(rstr)
        return req_to_dict(req)

    @staticmethod
    def return_currencies():
        req = pub_api_request('returnCurrencies')
        return req_to_dict(req)

    @staticmethod
    def return_loan_orders(currency):
        req = pub_api_request('returnLoanOrders&currency=' + currency)
        return req_to_dict(req)

    #Trading API#
    def priv_api_request(self, params):
        params['nonce'] = int(time())
        encoded_post_data = parse.urlencode(params).encode('utf8')
        signature = hmac.new(self.secret, encoded_post_data, hashlib.sha512).hexdigest()
        headers = {'Key' : self.key, 'Sign' : signature}
        req = urlreq.Request(trading_url, encoded_post_data, headers)
        return req_to_dict(req)

    def return_balances(self):
        p = command_param('returnBalances')
        return self.priv_api_request(params)

    def return_complete_balances(self, allBalances=False):
        p = command_param('returnCompleteBalances')
        if allBalances:
            p['account'] = 'all'
        return self.priv_api_request(p)

    def return_deposit_addresses(self):
        p = command_param('returnDepositAddresses')
        return self.priv_api_request(p)

    def generate_new_address(self, currency):
        p = command_param('generateNewAddress')
        p['currency'] = currency
        return self.priv_api_request(p)['response']

    def return_deposits_withdrawls(self, start, end):
        p = command_param('returnDepositsWithdrawals')
        p['start'] = str(start)
        p['end'] = str(end)
        return self.priv_api_request(p)

    def return_open_orders(self, currencyPair = 'all'):
        p = command_param('returnOpenOrders')
        p['currencyPair'] = currencyPair
        return self.priv_api_request(p)

    def return_trade_history(self, currencyPair = 'all', start = None, end = None):
        p = command_param('returnTradeHistory')
        p['currencyPair'] = currencyPair
        if start and end:
            p['start'] = str(start)
            p['end'] = str(end)
        return self.priv_api_request(p)

    def return_order_trades(self, orderNumber):
        p = command_param('returnOrderTrades')
        p['orderNumber'] = orderNumber
        return self.priv_api_request(p)

    def buy_or_sell(self, buy, currencyPair, rate, amount, fillOrKill = False, immediateOrCancel = False, postOnly = False):
        if buy:
            p = command_param('buy')
        else:
            p = command_param('sell')
        p['currencyPair'] = currencyPair
        p['rate'] = str(rate)
        p['amount'] = str(amount)
        if fillOrKill:
            p['fillOrKill'] = str(1)
        if immediateOrCancel:
            p['immediateOrCancel'] = str(1)
        if postOnly:
            p['postOnly'] = str(1)
        return self.priv_api_request(p)

    def buy(self, currencyPair, rate, amount, fillOrKill = False, immediateOrCancel = False, postOnly = False):
        self.buy_or_sell(True, currencyPair, rate, amount, fillOrKill, immediateOrCancel, postOnly)
        
    def sell(self, currencyPair, rate, amount, fillOrKill = False, immediateOrCancel = False, postOnly = False):
        self.buy_or_sell(False, currencyPair, rate, amount, fillOrKill, immediateOrCancel, postOnly)

    def cancel_order(self, orderNumber):
        p = command_param('cancelOrder')
        p['orderNumber'] = str(orderNumber)
        return self.priv_api_request(p)
        
    def move_order(self, orderNumber, rate, amount = None):
        p = command_param('moveOrder')
        p['orderNumber'] = str(orderNumber)
        p['rate'] = str(rate)
        if amount:
            p['amount'] = str(amount)
        return self.priv_api_request(p)

    def withdraw(self, currency, amount, address):
        p = command_param('withdraw')
        p['currency'] = currency
        p['amount'] = str(amount)
        p['address'] = address
        return self.priv_api_request(p)

    def return_fee_info(self):
        p = command_param('returnFeeInfo')
        return self.priv_api_request(p)

    def return_available_acct_balances(self):
        p = command_param('returnAvailableAccountBalances')
        return self.priv_api_request(p)

    def return_tradable_balances(self):
        p = command_param('returnTradableBalances')
        return self.priv_api_request(p)

    def transferBalance(self, currency, amount, fromAccount, toAccount):
        p = command_param('transferBalance')
        p['currency'] = currency
        p['amount'] = str(amount)
        p['fromAccount'] = fromAccount
        p['toAccount'] = toAccount
        return self.priv_api_request(p)

    def returnMarginAccountSummary(self):
        p = command_param('returnMarginAccountSummary')
        return self.priv_api_request(p)

    def margin_buy_or_sell(self, buy, currencyPair, rate, amount, lendingRate = None):
        if buy:
            p = command_param('marginBuy')
        else:
            p = command_param('marginSell')
        p['currencyPair'] = currencyPair
        p['rate'] = str(rate)
        p['amount'] = str(amount)
        if lendingRate:
            p['lendingRate'] = lendingRate
        return self.priv_api_request(p)

    def margin_buy(self, currencyPair, rate, amount, lendingRate = None):
        return margin_buy_or_sell(True, currencyPair, rate, amount, lendingRate)

    def margin_sell(self, currencyPair, rate, amount, lendingRate = None):
        return margin_buy_or_sell(False, currencyPair, rate, amount, lendingRate)

    def get_margin_position(self, currencyPair = 'all'):
        p = command_param('getMarginPosition')
        p['currencyPair'] = currencyPair
        return self.priv_api_request(p)

    def close_margin_position(self, currencyPair):
        p = command_param('closeMarginPosition')
        p['currencyPair'] = currencyPair
        return self.priv_api_request(p)

    def create_loan_offer(self, currency, amount, duration, autoRenew, lendingRate):
        p = command_param('createLoanOffer')
        p['currency'] = currency
        p['amount'] = str(amount)
        p['duration'] = str(duration)
        p['autoRenew'] = str(1) if autoRenew else str(0)
        p['lendingRate'] = str(lendingRate)
        return self.priv_api_request(p)

    def cancel_loan_offer(self, orderNumber):
        p = command_param('cancelLoanOffer')
        p['orderNumber'] = str(orderNumber)
        return self.priv_api_request(p)

    def return_open_loan_offers(self):
        p = command_param('returnOpenLoanOffers')
        return self.priv_api_request(p)

    def return_active_loans(self):
        p = command_param('returnActiveLoans')
        return self.priv_api_request(p)

    def return_lending_history(self, start = None, end = None):
        p = command_param('returnLendingHistory')
        if start and end:
            p['start'] = start
            p['end'] = end
        return self.priv_api_request(p)

    def toggle_auto_renew(self, orderNumber):
        p = command_param('toggleAutoRenew')
        p['orderNumber'] = str(orderNumber)
        return self.priv_api_request(p)
